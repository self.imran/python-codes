from django.shortcuts import render

from rest_framework import viewsets 

from .models import SampleModel
from .serializers import SampleSerializer

from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import(
	BasicAuthentication,
	SessionAuthentication,
	TokenAuthentication
)

class SampleViewSet(viewsets.ModelViewSet):
	#authentication_classes = [BasicAuthentication]
	authentication_classes = [TokenAuthentication]
	#authentication_classes = [SessionAuthentication]
	permission_classes = [IsAuthenticated]
	queryset = SampleModel.objects.all()
	serializer_class = SampleSerializer

