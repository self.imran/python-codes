from django.contrib import admin

from .models import SampleModel


@admin.register(SampleModel)
class LeadAdmin(admin.ModelAdmin):
        model = SampleModel

        list_display = (
            'id',
            'emp_name',
            'email', 
            'created_at',
            )
        list_filter = (
            "created_at",
            )

        search_fields = (
            "emp_name",
            )
        date_hierarchy = "created_at"
