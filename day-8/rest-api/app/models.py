from django.db import models

class SampleModel(models.Model):
	emp_name = models.CharField(max_length=100)
	email = models.EmailField()
	created_at = models.DateTimeField(auto_now_add=True)